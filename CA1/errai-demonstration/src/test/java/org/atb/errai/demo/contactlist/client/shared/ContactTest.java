package org.atb.errai.demo.contactlist.client.shared;

import junit.framework.TestCase;
import org.junit.Test;

public class ContactTest extends TestCase {


	public void testAttributtesAreValid() {
		// TODO
		Contact c=new Contact();

		assertTrue(c != null);
	}

    @Test
    public void getName1() {
        Contact contact = new Contact("Tiago", "tiago@gmail.com","91");
        contact.setName("Tiago");
        String expectedResult = contact.getName();
        String result = "Tiago";
        assertEquals(result,expectedResult);
    }


    @Test
    public void getEmail() {
        Contact contact = new Contact("Tiago", "12345@gmail.com","9111111");
        contact.setEmail("tiago@gmail.com");
        String expectedResult = contact.getEmail();
        String result = "tiago@gmail.com";
        assertEquals(result,expectedResult);
    }


    @Test
    public void getPhoneNumber() {
        Contact contact = new Contact("Tiago", "tiago@gmail.com","91");
        contact.setPhoneNumber("912225696");
        String expectedResult = contact.getPhoneNumber();
        String result = "912225696";
        assertEquals(result,expectedResult);
    }

    @Test
    public void validateEmailValidMail() {
        //Arrange
        Contact contact = new Contact();
        String emailValid = "andreiass02@mail.pt";
        String expectedResult = "andreiass02@mail.pt";
        //Act
        String result = contact.validateEmail(emailValid);
        //Assert
        assertEquals(expectedResult, result);
    }

    @Test
    public void notValidateEmail() {
        //Arrange
        Contact contact = new Contact();
        String emailNotValid = "andreiass02.mail.pt";
        String expectedResult = "* invalid email *";
        //Act
        String result = contact.validateEmail(emailNotValid);
        //Assert
        assertEquals(expectedResult, result);
    }

    @Test
    public void isEmptyEmail() {
        //Arrange
        Contact contact = new Contact();
        String emailNotValid = "";
        String expectedResult = "* invalid email *";
        //Act
        String result = contact.validateEmail(emailNotValid);
        //Assert
        assertEquals(expectedResult, result);
    }

}