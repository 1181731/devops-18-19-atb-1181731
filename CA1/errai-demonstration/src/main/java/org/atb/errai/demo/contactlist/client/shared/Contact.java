package org.atb.errai.demo.contactlist.client.shared;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import org.jboss.errai.common.client.api.annotations.Portable;
import org.jboss.errai.databinding.client.api.Bindable;

@Bindable
@Portable
@Entity
@NamedQueries({ @NamedQuery(name = Contact.ALL_CONTACTS_QUERY, query = "SELECT c FROM Contact c ORDER BY c.id") })
public class Contact implements Serializable, Comparable<Contact> {
	private static final long serialVersionUID = 1L;

	public static final String ALL_CONTACTS_QUERY = "allContacts";

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;

	private String name;
	private String email;
	private String phoneNumber;

	public Contact() {
	}

	public Contact(String name, String email, String phoneNumber) {
		this.name = name;
		this.email = email;
		this.phoneNumber = phoneNumber;
	}

	public Contact(long id, String name, String email, String phoneNumber) {
		this(name, email,phoneNumber);
		this.id = id;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) throws IllegalArgumentException {
		String emailValidate = validateEmail(email);
		this.email = emailValidate;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	@Override
	public String toString() {
		return "Contact{" +
				"id=" + id +
				", name='" + name + '\'' +
				", email='" + email + '\'' +
				", phoneNumber='" + phoneNumber + '\'' +
				'}';
	}

	@Override
	public int compareTo(Contact contact) {
		return (int) (id - contact.id);
	}

	@Override
	public boolean equals(final Object obj) {
		return (obj instanceof Contact) && ((Contact) obj).getId() != 0 && ((Contact) obj).getId() == getId();
	}

	@Override
	public int hashCode() {
		return (int) getId();
	}

	public String validateEmail(String email) {
		String EMAIL_REGEX = "^[\\w-\\.+]*[\\w-\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";

		if (email == null || email.isEmpty() == true) {
			email = "* invalid email *";
			return email;
		}

		if (email.matches(EMAIL_REGEX) == true) {
			return email;
		} else {
			email = "* invalid email *";
			return email;
		}
	}
}
