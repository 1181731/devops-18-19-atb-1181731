CA1 Demonstration
===================

ABRI A LINHA DE COMANDOS DO IDE:

1 - Comecei por aceder à pasta onde se encontrava o trabalho de DEVOPS CD C:\USERS\DRIII\DOCUMENTS\DEVOPSTRABALHO

2 - Coloquei a operação: git init para iniciar o repositório

4 - Depois disso, fiz clone do projeto através do comando: git remote add origin https://1181731@bitbucket.org/1181731/devops-18-19-atb-1181731.git para direcionar para o repositório

3 - Fiz o meu commit através da linha de comando: git commit -m "initial commit"

4 - Depois de aceder à pasta dei o nome à versão que se encontrava de MY VERSION V.1.2.0 com o comando : git tag -a v1.2.0 -m "My version v.1.2.0"

5 - Marquei a etiqueta com a seguinte linha de comando - git push origin v1.2.0

6 - De seguida criei uma branch demoninada de Phone-Field com a seguinte linha de comando : git checkout -b phone-field

7 - No Intellij IDEA fiz as alterações para a colocação do campo PhoneNumber, acedendo às seguintes pastas:
 - Contact 
 - NewContactPage.html
 - NewContactPage
 - ContactListPage.html
 - ContactDisplay

8 - Após essa alteração fiz novamente um commit para guardar as alterações feitas através do comando - git commit -m "update phone field"

9 - Após o commit fiz por fim um push com a linha de comandos - git push origin phone-field

10 - Fiz as modificações relativamente aos testes para a criação do contacto e após essas modificações fiz - git commit -a -m "tests"

11 - Após o commit fiz por fim um push com a linha de comandos - git push origin phone-field

12 - Para fazer a conexão do branch phone-field segui os seguintes passos:
	- git checkout master
	- git pull origin master
	- git merge phone-field
 	- git push origin master

13 - Após fazer o merge branch utilizei a linha de comando git branch para confirmar se efetivamente tinha juntado aparecendo-me a seguinte mensagem:
		C:\Users\driii\Documents\DevopsTrabalho>git branch
		* master
 		phone-field

14 - Depois disso fiz o tag à versão final git tag -a v1.3.0 -m "My version v.1.3.0" e depois disso git push origin v1.3.0

15 - Criei novamente um novo branch denominado de fix-invalid-email utilizando na linha de comandos git checkout -b fix-invalid-email

16 - Fiz, para verficar um git branch aparecendo a seguinte mensagem: 
	C:\Users\driii\Documents\DevopsTrabalho>git branch
	* fix-invalid-email
 	master
  	phone-field