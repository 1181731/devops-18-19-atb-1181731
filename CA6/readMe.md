-----------------------------------------------
-----------------------------------------------
Configuration Management with Ansible
-----------------------------------------------
-----------------------------------------------

---------------------------------------------------------------
--Ansible is a radically simple IT automation engine----------- 
--that automates cloud provisioning, configuration management,- 
--application deployment, intra-service orchestration,--------- 
--and many other IT needs.-------------------------------------
---------------------------------------------------------------

# Pipelines with Jenkins Demonstration

This project demonstrates how to automate a project with ansible

## Requirements

errai-demonstration-gradle.war
jenkins.war

https://github.com/atb/vagrant-with-ansible


## Create CA6 folder

Run the vagrant to create the 3 virtual boxes!

	$vagrant up (folder we have the project) //inicializa as tres maquinas v
	
Enter the virtual machine that have ansible

	$vagrant ssh ansible (folder we have the project) // entramos na maquina que tem o ansible
	
Change to the folder that have vagrant

	$cd /vagrant 

Execute ansible playbook

	$ansible-playbook playbook1.yml	// corremos o playbook que é um script que executa varias tarefas (exe o wildfly numa das maquinas host1)
		
		
## Add errai-demonstration

-----------------------------------------------
-----------------------------------------------
1. Copy errai demonstration from a previous CA
-----------------------------------------------
-----------------------------------------------

-----EDIT PLAYBOOK------

- Copy errai to host 1:

copy: remote_src=no src=/vagrant/errai-demonstration-gradle.war  //copiamos o errai da raiz para o host1
dest=/opt/wildfly/standalone/deployments/		
		
- Create a task on host2 to download and install H2 Database // criamos uma task para instalar o h2

- hosts: host2
become: yes 
tasks:  
- name: install H2     
get_url: url=http://repo2.maven.org/maven2/com/h2database/h2/1.4.199/h2-1.4.199.jar dest=/home/vagrant
- name: run H2 
shell: java -cp /home/vagrant/h2-1.4.199.jar org.h2.tools.Server -web -webAllowOthers -tcp -tcpAllowOthers -ifNotExists > ~/out.txt &

## Change Errai to save in database instead memory // mudamos o errai para gravar os dados no host2 em vez de guardar em memoria

- name: Change h2 database to host2
      replace:
        path: /opt/wildfly/standalone/configuration/standalone.xml
        regexp: '<connection-url>jdbc:h2:mem;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE</connection-url>'
        replace: '<connection-url>jdbc:h2:tcp://192.168.33.12:9092//vagrant/data;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE</connection-url>'
- name: Change password h2 database to host2
      replace:
        path: /opt/wildfly/standalone/configuration/standalone.xml
        regexp: '<password>sa</password>'
        replace: '<password></password>'

----Edit Vagrant file----- // editamos o vagrant file pondo o host2 a correr sempre antes do host 1 (para evitar erros de arranque ie arrancar a app sem base de dados pode dar erro)

run always H2 (before) host 1 run Errai

host2.vm.provision "shell", run: "always", inline: <<-SHELL
	   java -cp ./h2-1.4.199.jar org.h2.tools.Server -web -webAllowOthers -tcp -tcpAllowOthers -ifNotExists &
SHELL

## Install jenkings on host with ansible // instalar jenkins na maquina que tem o ansible

Using the vagrant file we install Jenkings on Host with ansible, when we start vagrant

SHELL //introduzo estes comandos no vagrant file para ele instalar o jenkins quando faço vagrant up
wget -q -O - http://pkg.jenkins-ci.org/debian/jenkins-ci.org.key | sudo apt-key add -
sudo sh -c 'echo deb http://pkg.jenkins-ci.org/debian binary/ > /etc/apt/sources.list.d/jenkins.list'
sudo apt-get update && sudo apt-get upgrade
sudo apt-get install -y jenkins
SHELL
		
## Add a new stage on jenkinsfile to run ansible

-----------------------------------------------
-----------------------------------------------
1. We use the previous jenkings file and add a new stages to run ansible
-----------------------------------------------
-----------------------------------------------
	
pipeline {
    agent any

    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git credentialsId: 'Credentials', url: 'https://1181731@bitbucket.org/1181731/devops-18-19-atb-1181731_v3.git'
            }
        }
        
        stage('Assemble') {
            steps {
                echo 'Assembling...'
                bat 'cd PART2 && gradlew clean assemble'
            }
        }
        
        stage('Test') {
            steps {
                echo 'Testing...'
                bat 'cd PART2 && gradlew test'
                junit '**\\test-results\\test\\*.xml'
            }
        }
        
         stage('Javadoc') {
            steps {
                echo 'Javadoc...'
                bat 'cd PART2 && gradlew javadoc'
            }
        }
        
        stage('Archiving') {
            steps {
                echo 'Archiving...'
                archiveArtifacts 'PART2/build/libs/*'
            }
		}
        
		stage('Publish image') {
            steps {
				echo 'Publishing image...'
                 script {
                    dockerImage = docker.build ("1070189/ca5-part2:$BUILD_NUMBER","part2/errai-demonstration-gradle-master").push()
             }
		  }
	    }
		//introduzi estes dois stages para correr o ansible 
		stage('Ansible Init') {
            steps {
                script {    
               def tfHome = tool name: 'Ansible'
                env.PATH = "${tfHome}:${env.PATH}"
                 sh 'ansible --version'       
            }
            }
        }
        stage('Ansible Deploy') {   
            steps {    
              dir('dev/ansible'){ 
               sh 'ansible all -m ping -i hosts'    
            }
            }
        }
     }
  }
-----------------------------------------------
-----------------------------------------------//não consegui correr o jenkins file porque nao conseguia aceder ao meu repositório, dava-me erro nas credenciais
 2. Create a new project on jenkings! Identify the repository, credentials and location of Jenkings file!
 -----------------------------------------------
 -----------------------------------------------
 
 -----------------------------------------------
 -----------------------------------------------
 3. Do the build
 -----------------------------------------------
 -----------------------------------------------
  
	
## Last Commit and tag

Finally add the tag for this new version: ca6
 
         $ git tag -a ca6 -m "ca6-part1"
         $ git push origin ca6
          
And we do the last commit

        $ git commit -a -m "close CA6"
        $ git push origin master	


