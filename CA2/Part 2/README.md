CA2 - part2
===================

This is a demo application that demonstrates several Errai technologies.

Errai is a GWT-based framework for building rich web applications using next-generation web technologies. See [Errai](http://erraiframework.org).

GWT is a development toolkit for building and optimizing complex browser-based applications. With GWT, client and server parts of a web application are written in Java. 
See [GWT Project](http://www.gwtproject.org). 

This demonstration is based on [errai-jpa-demo-todo-list](https://github.com/errai/errai/tree/master/errai-demos/errai-jpa-demo-todo-list) 
but it has been updated to a "contact list application" and simplified as much as possible. 

## Prerequisites

 * Java JDK 8
    * You must download and properly install JDK 8 in your system   
 * Gradle
    * Gradle is downloaded when you use **gradlew**
 * WildFly 15.0.1.Final 
    * Wildfly is downloaded using the task **provision**

##First Step - Introduction

Add a new branch:

    $ git checkout -b gradle-plantuml

##Second Step - If you manually copy the image files from build/puml into build/docs/javadoc (do not forget the subfolders) and refresh the browser you will see the diagrams. How could this task be automated with gradle?

At the new branch add the task to solve the problem:

        task copyPlantUml(type: Copy) {
        from 'build/puml'
        into 'build/docs/javadoc'
        include '**/*.png'
         }
         
Commit and push the changes

        git commit -a -m "Fixes#11"
        git commit -a -m "Fixes#12"
        git push origin gradle-plantuml
      
##Third Step - Bug Fixes

Start creating the folder assets, after that use the following plugin, to create the .png 
images of sequence and classes diagrams.
 
 Add it on build.gradle
 
            id "io.kristiansen.gradle.PlantUMLPlugin" version "0.0.1"
    
 after that text the command RenderPlantUml
 
            gradlew RenderPlantUml
            
 Then use the the following instruction to build javadoc:
 
            gradlew javadoc
            
 Then copy the .png files to javadoc folder on your project
 
             task copyPlantUml(type: Copy) {
            from 'assets'
            into 'build/docs/javadoc'
            include '**/*.png'}
            
 Finally use the following code to update on web page, on the overview-summary:
 
            <h2>Class Diagram</h2>
            <img src="class-diagram.png"></a><br/>
            <h2>Sequence Diagram</h2>
            <img src="sequence-diagram.png"></a><br/></div>
            </div>
            
 After finishing this steps please commit and push the changes!
 
 Commit and push the changes
 
            git commit -a -m "Fixes#11"
            git commit -a -m "Fixes#12"
            git push origin gradle-plantuml   
        

    
    
    
    






                                                                                                        