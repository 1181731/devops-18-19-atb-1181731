CA2 Part 1
===================

This is a demo application that implements a basic multithreaded chat room server.

The server supports several simultaneous clients through multithreading. When a client connects the server requests a screen name, and keeps requesting a name until a unique one is received. After a client submits a unique name, the server acknowledges it. Then all messages from that client will be broadcast to all other clients that have submitted a unique screen name. A simple "chat protocol" is used for managing a user's registration/leaving and message broadcast.


Prerequisites
-------------

 * Java JDK 8
 * Apache Log4J 2
 * Gradle 5 (if you do not use the gradle wrapper in the project)
   

Build
-----

To build a .jar file with the application:

    % ./gradlew build 

Run the server
--------------

Open a terminal and execute the following command from the project's root directory:

    % java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp <server port>

Substitute <server port> by a valid por number, e.g. 59001

Run a client
------------

Open another terminal and execute the following gradle task from the project's root directory:

    % ./gradlew runClient

The above task assumes the chat server's IP is "localhost" and its port is "59001". If you whish to use other parameters please edit the runClient task in the "build.gradle" file in the project's root directory.

To run several clients, you just need to open more terminals and repeat the invocation of the runClient gradle task

Downloading and commit to your repository the example application 
------------

https://bitbucket.org/luisnogueira/gradle_basic_demo/

Read the instructions available in the readme.md file and experiment with the application
------------

% ./gradlew build

% java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp <1181731>

% ./gradlew runClient

Add a new task to execute the server
------------

add task runServer at basic_demo.gradle file, 
note that we miss args localhost as in task runClient
main = 'basic_demo.ChatServerApp' changed too

after 

gradlew runServer
other terminal gradlew runClient

Add a simple unit test and update the gradle script so that it is able to execute the test
------------

create packages test,java
create AppTest class
Copy test template for AppTest
Note that the unit tests require junit 4.12 to execute.
add this dependencies testCompile 'junit:junit:4.12' at basic_demo.gradle file

Add a new task of type Copy to be used to make a backup of the sources of the application
------------

create backUpApp directory at src level
add copy task to basic_demo.gradle file
task copyTask(type: Copy) {
        from 'src'
        into 'backupApp'
    }
    
gradlew copyTask
$ git add -A
$ git commit -a -m "Fixes #5"
$ git push

Add a new task of type Zip to be used to make an archive (i.e., zip file) of the sources of the application
------------

create backUpZip directory at src level
add copy task to basic_demo.gradle file
task backUpZip (type: Zip) {
    archiveFileName = "zipFile.zip"
    destinationDirectory = file("backupZip")
    from "src"
}

gradlew copyTask
$ git add -A
$ git commit -a -m "Fixes #6"
$ git push


At the end of the part 1 of this assignment mark your repository with the tag ca2-part1
------------

$ git tag -a ca2-part1 -m "ca2-part1"
$ git push origin ca2-part1
$ git add -A
$ git commit -a -m "Fixes #7"
$ git push
git


