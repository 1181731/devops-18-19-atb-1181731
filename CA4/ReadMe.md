CA4
===================
##1.
-Created a directory to CA4


    $ cd C:\DevOps\devops-18-19-atb-1181731
    $ git init

-Downloaded zip file docker-compose-demo from GitHub
(https://github.com/atb/docker-compose-demo

-Created issues in Bitbucket for all the tasks in this Class Assignment.

-Added all files to the Bitbucket repository with the following commands in the Git Bash:
    
    $ git add .
    $ git commit -a -m "CA4 initialization"
    $ git push origin master

-Installed Docker from https://docs.docker.com/v17.09/toolbox/toolbox_install_windows/
Since we're using Windows 10 Home, we installed Docker Toolbox.

##2.
-Opened the terminal and executed the two following commands:

    $ docker-compose build
    $ docker-compose up

-After this, to try out the H2 console, Wildfly and the Errai Demonstration, I needed to know the docker machine ip, so I executed the command

    $ docker-machine ip


-Next, on three different tabs in the browser, I insered the three following links:

	192.168.99.100:8080 (The ip and the port to access Wildfly

![alt text](https://i.imgur.com/1nAB3Qm.png)


    192.168.99.100:8082 (The ip and the port to access H2 console)

![alt text](https://i.imgur.com/0aHcrGZ.png)

	192.168.99.100:8080/errai-demonstration-gradle(The ip and the port to access the Errai Demonstration app)

![alt text](https://i.imgur.com/joF5IFo.png)

As we can see in the images, it was possible to access the H2 console ( besides being empty), it was also possible to access Wildfly but it was not possible to access the errai-demonstration app.
So, I had to make some changes.

-Added the .war file of errai-demonstration gradle

-After that, inside the web folder I added the following lines to the dockerfile

     && \\
	 sed -i 's\#\<connection-url\>jdbc:h2:mem:test;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE\</connection-url\>\#\<connection-url\>jdbc:h2:file://usr/src/data;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE\</connection-url\>\#g'
     /usr/src/app/wildfly-15.0.1.Final/standalone/configuration/standalone.xml && \\
     sed -i 's\#\<password\>sa\</password\>\#\<password\>\</password\>\#g'
     /usr/src/app/wildfly-15.0.1.Final/standalone/configuration/standalone.xml
     COPY /errai-demonstration-gradle.war
     /usr/src/app/wildfly-15.0.1.Final/standalone/deployments/
	 
-Changed the file docker-compose.yml so that the db runs first and we don't have time out.
	 	 
-After this, I could run the app

    $ docker-compose build
    $ docker-compose up


    192.168.99.100:8080/errai-demonstration-gradle

##3.
-After that, it was necessary to add persistence at the database so that when I exit the app I will still have the contacts saved.

-In the dockerfile:
    
	 sed -i 's#<connection-url>jdbc:h2:mem:test;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE</connection-url>#<connection-url>jdbc:h2:tcp://db:9092//usr/src/data/data;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE</connection-url>#g' 

-Then, I executed again

    $ docker-compose build
    $ docker-compose up

-In the H2 Console, in JBDC URL: 
     jdbc:h2:tcp://db:9092//usr/src/data/data;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE

	 
##4.
-For this final step, I had to create an account in DockerHub

-In the docker terminal:
 
    $ docker login

-I insered my credentials

-After that, to see the images I made the following command line:

    $ docker images

-To tag and update the repository, I made the following commands:
    
    $ docker tag 40d19523279e 1181731/ca4_web:web
    $ docker push 1181731/ca4_web


    $ docker tag f50ccbdace73 1181731/ca4_db:db
    $ docker push 1181731/ca4_db
	 
They are available on the following website: 

	https://hub.docker.com/?namespace=1181731
	
