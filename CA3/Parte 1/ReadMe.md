CA3
===================
Part 1
-------------
## VirtualBox - Install and Experimentation

-------------------

Before doing Class Assignment 3 - Part1, I started by installing VirtualBox on my system. Once installed, I also downloaded the ISO file Ubuntu 18.04, and installed it in VirtualBox. After having installed all the necessary tools for this operation, I clone the errai project and I ran some commands:

	 ./gradlew provision
	 ./gradlew build
     ./gradlew startWildfly
	 

After the modifications were completed, I opened the link available ''http://192.168.56.100:8080/errai-demonstration-gradle" with the change on IP, since I already have some program on my windows running on the chosen IP (in instead of 100 I had to put 55).	 

###Step 1

Still in VirtualMachine I made the following command:

	 git clone https://1181731@bitbucket.org/1181731/devops-18-19-atb-1181731.git
	 
With this last line, I'm making a clone of my entire repository for the Virtual Machine. Then I went to the folder CA1 and then put the following command:

	 mvn gwt:run
	 
When placing this command, Virtual Machine asked me to install maven, since it had not yet maven installed. I put:

	apt install maven

After installing maven, I made the following command in the CA1 folder:

	./gradlew build
	
The following error appeared	
	
	 Failed to execute goal on project errai-demonstration : Could not resolve dependencies for project org.atb.errai.demos:errai-demonstration:war:1.1.0-SNAPSHOT: Could not find artifact sun.jdk:jconsole:jar:jdk at specified path /usr/lib/jvm/java-8-jdk-amd64/jre/../lib/jconsole.jar

This error happens because Virtual Machine has no graphic interface and can not build with maven	
	
##Step 2    

In the folder CA2 - part1, and since it is a gradle project I made the following command:
	 
	 apt install gradle

After that I went to the build.gradle file to comment on the zip task since I realized that we could not build the gradle with this task. After this modification, I then made the gradle project build with the command:

     gradle build

After the build was done, I then proceeded to the execution of the chat, with the following command:

     gradle runServer
	 
In IDEA, in the task of 'RunClient' and instead of localhost I put 192.168.56.55

     task runClient(type:JavaExec, dependsOn: classes){
     classpath = sourceSets.main.runtimeClasspath

     main = 'basic_demo.ChatClientApp'

     args '192.168.56.55', '59001'
     }
	 
On the command line after these changes I went to the same folder "luis-nogueira-gradle" -> part1 and just wrote the following command:

	 gradlew runClient

The chat appeared immediately.

