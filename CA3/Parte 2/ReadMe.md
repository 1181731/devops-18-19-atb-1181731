CA3
===================

Part 3
-------------

## Vagrant - Introduction to Vagrant

Vagrant is a tool for building and managing virtual machine environments in a single workflow. With an easy-to-use workflow and focus on automation, Vagrant lowers development environment setup time, increases production parity, and makes the "works on my machine" excuse a relic of the past.
Vagrant is designed for everyone as the easiest and fastest way to create a virtualized environment!

### Vagrant - Why ?

Vagrant provides easy to configure, reproducible, and portable work environments built on top of industry-standard technology and controlled by a single consistent workflow to help maximize the productivity and flexibility of you and your team.

To achieve its magic, Vagrant stands on the shoulders of giants. Machines are provisioned on top of VirtualBox, VMware, AWS, or any other provider. Then, industry-standard provisioning tools such as shell scripts, Chef, or Puppet, can automatically install and configure software on the virtual machine.

### Simple experimentation of Vagrant

Before starting Class Assignment 3 - Part2, I started by installing the Vagrant and making the specifications told, by the teacher in the PDF "Lecture 06 -
Introduction to Vagrant "

##Step 1

After installing Vagrant, I went directly to the windows command line and created a folder named "vagrant-project-1" with the following command:

	 mkdir vagrant-project-1
	 cd vagrant-project-1


After created folder, and so that Vagrant could create the virtual machine, it was necessary to create a VagrantFile with the following command:

	 vagrant init envimation/ubuntu-xenial

##Step 2

To add a box to the VagrantFile file, the following command was made:

	 vagrant box add envimation/ubuntu-xenial http://www.dei.isep.ipp.pt/~alex/publico/ubuntu-xenial-virtualbox.box

This adds a box with this link to Vagrant. 

Then I started the virtual machine with the following command:

     vagrant up

After that, I started the virtual machine session with the following command:
	 
	 vagrant ssh

Finally, I turned off my virtual machine with the command:

	 vagrant halt

##The goal of Part 2 of this assignment is to use Vagrant to setup a virtual environment to execute the Errai Demonstration Web application

##Step 1

I copied the VagrantFile from the vagrant-multi-demo-master folder to the errai-demonstration-gradle-master folder.
I ran the IDEA as an administrator (so you can make necessary changes) to the errai-demonstration-gradle-master project. 
I ran the following commands from the Read Me file: 

	 gradlew clean
	 gradlew provision
	 gradlew build


With this last command (gradlew build) the build folder was created within the errai-demonstration-gradle-master project. Inside this folder exists a subfolder called 'libs' where the errai-demonstration-gradle-master.war file was created. I changeded the name of file for 'errai-demonstration-gradle.war' because the name had either the same as the one in the vagrantFile file :

	  web.vm.provision "shell", :run => 'always', inline: <<-SHELL
      # We assume that errai-demonstration-gradle.war is located in the host folder that contains the Vagrantfile
      sudo cp /vagrant/errai-demonstration-gradle.war /home/vagrant/wildfly-15.0.1.Final/standalone/deployments/errai-demonstration-gradle.war
      # So that the wildfly server always starts
      sudo nohup sh /home/vagrant/wildfly-15.0.1.Final/bin/standalone.sh > /dev/null &
      SHELL


After the .war file was created, I also removed it from the libs folder so that it stays at the same level as the vagrantFile file.

##Step 2    

After this, and to do part 3 of the work I started by in the IDEA, in the persistence.xml file by changing the following line:

	 <property name="hibernate.hbm2ddl.auto" value="update"/>


After that, and because I had to change the Vagrant File file so that the created contacts would remain in the database, even in memory, I changed the following line:

	 web.vm.provision "shell", :run => 'always', inline: <<-SHELL
      # We assume that errai-demonstration-gradle.war is located in the host folder that contains the Vagrantfile
      sudo cp /vagrant/errai-demonstration-gradle.war /home/vagrant/wildfly-15.0.1.Final/standalone/deployments/errai-demonstration-gradle.war
      # So that the wildfly server always starts
     sudo nohup sh /home/vagrant/wildfly-15.0.1.Final/bin/standalone.sh > /dev/null &
      sudo rm errai-demonstration-gradle.war.failed
	  sed -i 's#<connection-url>jdbc:h2:file:m:test</connection-url>#<connection-url>jdbc:h2:tcp://192.168.33.11:9092/~/test</connection-url>#g' /home/vagrant/wildfly-15.0.1.Final/standalone/configuration/standalone.xml
	 sed -i 's#<password>sa</password>#<password></password>#g' /home/vagrant/wildfly-15.0.1.Final/standalone/configuration/standalone.xml
	
     SHELL
     end

So the contacts were stored in the database, but they were not yet persistent, that is, when I turned off Virtual Machine, everything was deleted.
In the final part of this work, I was not able to persist the data in a shared Folder despite trying in several ways. I realized that it would be in the part where the 'm' is, changing by the way to the folder, but without effect ...

     web.vm.provision "shell", :run => 'always', inline: <<-SHELL
      # We assume that errai-demonstration-gradle.war is located in the host folder that contains the Vagrantfile
      sudo cp /vagrant/errai-demonstration-gradle.war /home/vagrant/wildfly-15.0.1.Final/standalone/deployments/errai-demonstration-gradle.war
      # So that the wildfly server always starts
      sudo nohup sh /home/vagrant/wildfly-15.0.1.Final/bin/standalone.sh > /dev/null &
	  sudo rm errai-demonstration-gradle.war.failed
	  sed -i 's#<connection-url>jdbc:h2:file:C:/Users/driii/Documents/DevopsTrabalho/CA3/Part2/errai-demonstration-gradle-master/SharedVMFolder<       /connection-url>#<connection-url>jdbc:h2:tcp://192.168.33.11:9092/~/test</connection-url>#g' /home/vagrant/wildfly-15.0.1.Final/standalone/configuration/standalone.xml
	 sed -i 's#<password>sa</password>#<password></password>#g' /home/vagrant/wildfly-15.0.1.Final/standalone/configuration/standalone.xml
	
    SHELL
     end
